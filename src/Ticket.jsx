import React from "react";
import styled from 'styled-components';

const Units = styled.h4`
    font-size: 2em;
    font-weight: 700;
    color: white;
    margin-left: 10px
`

const Buy = styled.div`
    width: 300px;
    height: 100px;
    border-radius: 20px;
    margin: 2px;
    background-color: mediumseagreen;
    display: flex;
    justify-content: space-around;
    align-items: center;
    font-size: 0.9em;
    color: darkslategrey;
`

const Total = styled.div`
    width: 70%;
    height: 100px;
    border-radius: 20px;
    background-color: darkslategrey;
    color: lightgreen;
    font-size: 3em;
    display: flex;
    justify-content: space-around;
    align-items: center;
    margin: 10px auto;
`

export default (props) => {

    const fruitBuyList = props.buyList.map(el => {

        const totalPrice = Number((el.units * el.preu).toFixed(2));

        if (el.units !== 0) {

            return (
                <Buy>
                    <Units> x{el.units}</Units>
                    <div className="item">
                        <h3>{el.nom}</h3>
                        <p>{el.units} ud <b>x</b> {el.preu} €/ud</p>
                        <strong>Total {el.nom}: {totalPrice} €</strong>
                    </div>
                    <div className="btn-trash" onClick={() => props.deleteFruit(el.id)}>
                        <i className="fa fa-trash bin" ></i>
                    </div>
                </Buy>
            )
        }
    })

    return (
        <>
            <h2>Carrito</h2>
            <div className="carrito">
                {fruitBuyList}
            </div>
            <Total>
                <i className="fa fa-shopping-cart"></i>
                <span>Total:</span>
                <strong>{props.total} €</strong>
            </Total>
        </>
    )
}