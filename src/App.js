import React, { useEffect, useState } from "react";

import { Container, Row, Col } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

import PRODUCTS from "./datos.json";
import Lista from "./Lista";
import Ticket from "./Ticket";

function App() {

  const [fruitList, setFruitList] = useState(PRODUCTS);
  const [total, setTotal] = useState(0);

  //AÑADIR PRODUCTO AL CARRITO

  const addFruit = (id) => {

    const newFruitList = fruitList.map(el => {

      if (el.id === id) {
        el.units = el.units + 1;
      }
      return el;

    });

    setFruitList(newFruitList);
  }


  //ELIMINAR PRODUCTO DEL CARRITO

  const deleteFruit = (id) => {

    const newFruitList = fruitList.map(el => {

      if (el.id === id) {
        el.units = 0;
      }
      return el;

    });

    setFruitList(newFruitList);
  }


  //EFFECT - CALCULAR TOTAL

  useEffect(() => {

    const frutasActivas = fruitList.filter(el => el.units !== 0)

      .map(el => {
        el.cost = el.units * el.preu;
        return el;
      })

    let totalPrice = 0;

    for (let i = 0; i < frutasActivas.length; i++) {
      totalPrice = totalPrice + frutasActivas[i].cost;
    }

    let roundedPrice = Number((totalPrice).toFixed(2))

    setTotal(roundedPrice);

  }, [fruitList])

  return (
    <>
      <div className="title-box">
        <h1>
          <i className="fa fa-leaf fa-flip-horizontal leaf"></i>
            Fruteria Pol
          <i className="fa fa-leaf leaf"></i>
        </h1>
      </div>
      <Container>
        <Row className="m-2">
          <Col sm="5" className="d-flex flex-wrap justify-content-center shop">
            <Lista data={fruitList} addFruit={addFruit} />
          </Col>
          <Col sm="7" className="d-flex flex-column">
            <Ticket buyList={fruitList} deleteFruit={deleteFruit} total={total} />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default App;
