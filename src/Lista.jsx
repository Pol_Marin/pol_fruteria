import React from "react";
import { Button, Card, CardTitle, CardText } from "reactstrap"
import styled from 'styled-components';

const ProductTitle = styled.h3`
    font-size: 2.2em;
    font-weight: 700;
    color: white;
`

export default (props) => {

    const totalProductos = props.data.map(el => {

        return (
            <div className="product-box">
                <ProductTitle> {el.nom}</ProductTitle>
                <p>{el.preu} €/ud</p>
                <Button color="success" onClick={() => props.addFruit(el.id)}>AÑADIR</Button>
            </div>
        )
    })
    return (
        <>
            <h2>Productos</h2>
            {totalProductos}
        </>
    )
}